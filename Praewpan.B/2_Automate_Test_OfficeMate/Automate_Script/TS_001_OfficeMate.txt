*** Settings ***
Resource          ../Resource/AllKeywords.txt

*** Test Cases ***
TC_001_Canon_Calculate_Lowest_Price
    OfficeMate_OpenBrowserOfficeMate
    ${count_product}    OfficeMate_FindCalculateLowestPrice
    ${data_product_name}    ${data_product_id}    ${amount_product}    OfficeMate_AddCalculatorsLowestPriceToCart    ${count_product}
    OfficeMate_VerifyItemsInCart    ${data_product_name}    ${data_product_id}    ${amount_product}
