*** Settings ***
Library           Collections

*** Test Cases ***
01_Get_Duplicate_Number_List
    [Documentation]    1. The array A contains all numbers between 1 and 1000 and is not a sorted array.
    ...
    ...    However, there are 2 numbers that are duplicated. Please find these 2 numbers by your preferred computer language and don’t worry about the code syntax.
    ...
    ...    For example; the data of this array can be; A = {1, 15, .., 45, .., 15, .., 45, 786} ; A.Size() is 1002
    ${list_number}    Generate Random Number
    ${list_duplicate_number}    Get List Duplicate Number    ${list_number}
    Log List    ${list_duplicate_number}

*** Keywords ***
Get List Duplicate Number
    [Arguments]    ${list_number}
    ${list_checked_number}    Create List
    ${list_duplicate_number}    Create List
    : FOR    ${number}    IN    @{list_number}
    \    ${number_is_duplicate}    Run Keyword And Return Status    List Should Contain Value    ${list_checked_number}    ${number}
    \    Run Keyword If    ${number_is_duplicate} == ${True}    Append To List    ${list_duplicate_number}    ${number}
    \    ...    ELSE    Append To List    ${list_checked_number}    ${number}
    Log List    ${list_duplicate_number}
    [Return]    ${list_duplicate_number}

Generate Random Number
    ${list_number}    Evaluate    random.sample(range(1, 1001), 1000)    random
    Append To List    ${list_number}    ${15}    ${45}
    Log List    ${list_number}
    [Return]    ${list_number}
