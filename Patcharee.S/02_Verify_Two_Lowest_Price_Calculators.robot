﻿*** Settings ***
Library           Collections
Library           Selenium2Library
Library           Process

*** Variables ***
${General_TimeOut}    5s
${Brand}          Canon
${Category}       เครื่องคิดเลข
${canvas_Loading}    xpath=//div[@data-testid="pnl-loading"]
${url_Officemate}    https://www.officemate.co.th
${popup_Subscription}    xpath=//div[@id="pnl-SubScription-Popup"]
${button_ThaiLanguage}    xpath=//div[@id="app"]//div[text()="th"]
${text_Login_ThaiLanguage}    xpath=//a[text()="เข้าสู่ระบบ"]
${button_Subscription_Close}    xpath=//div[@id="pnl-SubScription-Popup"]//div[text()="กรอกอีเมล"]/../span[text()="X"]
${textbox_Search}    xpath=//input[@data-testid="txt-SearchBar"]
${button_Search}    xpath=//div[@id="btn-searchResultPage"]
${button_SortBy}    xpath=//div[@data-testid="pnl-productGrid"]/..//div[text()="จัดเรียงตาม"]
${button_SortBy_ASCPrice}    xpath=//div[@data-testid="pnl-productGrid"]/..//div[@id="price_asc"]
${button_Cart}    xpath=//div[@data-testid="btn-MiniCart"]
${button_ViewCartPage}    xpath=//div[@id="mini-cart"]//div[text()="ดูตะกร้าสินค้า"]
${text_Shipment_EstimateDate}    id=lbl-ShipmentGroup-EstimateDate

*** Test Cases ***
02_Verify_Two_Lowest_Price_Calculators
    [Documentation]    2. Create an automated test script with the following steps. Any opensource tools and any programming language are ok. e.g. selenium, RBFW, cypress and so on. Once done, please upload the code to any public repository as you like and provide us the way to access the public repository.
    ...
    ...    - Open https://www.officemate.co.th website
    ...    - Find the canon calculator with the lowest price available on the officemate website
    ...    - Add 2 lowest price calculators into cart
    ...    - Open cart page and verify whether there are 2 items in cart
    # Open https://www.officemate.co.th website
    Open Office Mate Website
    # Find the canon calculator with the lowest price available on the officemate website
    Clear Subscription Popup
    Select Thai Language
    Search Product by Brand    ${Brand}
    Filter Product by Category and Brand    ${Category}    ${Brand}
    Sort Product by Ascending Price
    # Add 2 lowest price calculators into cart
    Add Two Lowest Price Calculators Into Cart
    # Open cart page and verify whether there are 2 items in cart
    Go To Cart Page
    Verify Items In Cart
    [Teardown]    Run Keywords    Run Keyword If Test Passed    Close Firefox Browser
    ...    AND    Run Keyword If Test Failed    Capture Page Screenshot
    ...    AND    Close Firefox Browser

*** Keywords ***
Click Web Element
    [Arguments]    ${Locator}    ${Timeout}=${General_TimeOut}
    ${Result}    BuiltIn.Run Keyword And Return Status    Selenium2Library.Wait Until Element Is Visible    ${Locator}    ${Timeout}
    BuiltIn.Run Keyword If    '${result}'=='False'    Selenium2Library.Wait Until Page Contains Element    ${Locator}    ${Timeout}
    Wait Until Keyword Succeeds    5x    1s    Selenium2Library.Click Element    ${Locator}

Close Firefox Browser
    Kill Gecko Driver Process
    Close All Browsers

Input Web Text
    [Arguments]    ${Locator}    ${Text}    ${Timeout}=${General_TimeOut}
    ${result}    BuiltIn.Run Keyword And Return Status    Selenium2Library.Wait Until Element Is Visible    ${Locator}    ${Timeout}
    BuiltIn.Run Keyword If    '${result}'=='False'    Wait Web Until Page Contains Element    ${Locator}    ${Timeout}
    Wait Until Keyword Succeeds    5x    1s    Selenium2Library.Input Text    ${Locator}    ${Text}

Get Web Text
    [Arguments]    ${Locator}    ${Timeout}=${General_TimeOut}
    ${result}    BuiltIn.Run Keyword And Return Status    Selenium2Library.Wait Until Element Is Visible    ${Locator}    ${Timeout}
    BuiltIn.Run Keyword If    '${result}'=='False'    Wait Until Page Contains Element    ${Locator}    ${Timeout}
    ${Text}    Wait Until Keyword Succeeds    5    1    Selenium2Library.Get Text    ${Locator}
    [Return]    ${Text}

Get Web Value
    [Arguments]    ${Locator}    ${Timeout}=${General_TimeOut}
    ${result}    BuiltIn.Run Keyword And Return Status    Selenium2Library.Wait Until Element Is Visible    ${Locator}    ${Timeout}
    BuiltIn.Run Keyword If    '${result}'=='False'    Selenium2Library.Wait Until Page Contains Element    ${Locator}    ${Timeout}
    Wait Until Keyword Succeeds    5x    1s    Run Keywords    Selenium2Library.Wait Until Element Is Visible    ${Locator}
    ...    AND    Selenium2Library.Wait Until Element Is Visible    ${Locator}    ${Timeout}
    ${valueActual}    Selenium2Library.Get Value    ${Locator}
    [Return]    ${valueActual}

Web Element Should Be Visible
    [Arguments]    ${Locator}    ${Timeout}=${General_TimeOut}
    ${result}    Run Keyword And Return Status    Selenium2Library.Wait Until Page Contains Element    ${Locator}    ${Timeout}
    ${result}    Run Keyword And Return Status    Selenium2Library.Wait Until Element Is Visible    ${Locator}    ${Timeout}
    Selenium2Library.Element Should Be Visible    ${Locator}

Web Element Should Be Not Visible
    [Arguments]    ${Locator}    ${Timeout}=${General_TimeOut}
    ${result}    Run Keyword And Return Status    Element Should Not Be Visible    ${Locator}
    Run Keyword If    ${result} == ${False}    Wait Until Element Is Not Visible    ${Locator}    ${Timeout}

Open Office Mate Website
    Open Browser    ${url_Officemate}    ff
    Set Window Size    1600    900

Wait For Loading Hide
    Run Keyword And Ignore Error    Wait Until Element Is Visible    ${canvas_Loading}
    Run Keyword And Ignore Error    Wait Until Element Is Not Visible    ${canvas_Loading}

Kill Gecko Driver Process
    Run Keyword And Ignore Error    Close All Browsers
    Run Process    taskkill /f /im geckodriver.exe    shell=True

Clear Subscription Popup
    Web Element Should Be Visible    ${popup_Subscription}
    Click Web Element    ${button_Subscription_Close}
    Comment    Press Keys    None    ALT+N

Select Thai Language
    Click Web Element    ${button_ThaiLanguage}
    Web Element Should Be Visible    ${text_Login_ThaiLanguage}

Search Product by Brand
    [Arguments]    ${brand}
    Input Web Text    ${textbox_Search}    ${brand}
    Click Web Element    ${button_Search}

Filter Product by Category and Brand
    [Arguments]    ${category}    ${brand}
    # Filter by Category
    Click Web Element    xpath=//div[@id="btn-collapseClose-Category"]/..//div[text()="${category}"]
    Wait For Loading Hide
    # Filter by Brand
    Click Web Element    xpath=//div[@id="btn-collapseClose-Brand"]/..//div[text()="${brand}"]
    Wait For Loading Hide

Sort Product by Ascending Price
    Click Web Element    ${button_SortBy}
    Click Web Element    ${button_SortBy_ASCPrice}
    Wait For Loading Hide

Add Two Lowest Price Calculators Into Cart
    Sleep    3s    # Work Around
    : FOR    ${i}    IN RANGE    1    3
    \    ${get_cart_quantity_before}    Get Text    id=lbl-minicartQty
    \    ${expect_cart_quantity}    Evaluate    ${get_cart_quantity_before} + 1
    \    Mouse Over    xpath=//div[@data-testid="pnl-productGrid"]/div[${i}]
    \    Click Web Element    xpath=//div[@data-testid="pnl-productGrid"]/div[${i}]//span[text()="ใส่ในตะกร้า"]
    \    Web Element Should Be Visible    xpath=//div[@data-testid="pnl-productGrid"]/div[${i}]//span[text()="กำลังเพิ่ม"]
    \    Web Element Should Be Not Visible    xpath=//div[@data-testid="pnl-productGrid"]/div[${i}]//span[text()="กำลังเพิ่ม"]
    \    Sleep    2s    # Work Around
    \    ${actual_cart_quantity}    Get Web Text    id=lbl-minicartQty
    \    Capture Page Screenshot
    \    Should Be Equal As Integers    ${expect_cart_quantity}    ${actual_cart_quantity}

Go To Cart Page
    Wait Until Keyword Succeeds    5x    2s    Run Keywords    Click Web Element    ${button_Cart}
    ...    AND    Click Web Element    ${button_ViewCartPage}
    ...    AND    Web Element Should Be Visible    ${text_Shipment_EstimateDate}

Verify Items In Cart
    ${expect_cart_quantity}    Set Variable    2
    ${actual_cart_quantity}    Set Variable    0
    : FOR    ${i}    IN RANGE    1    3
    \    ${get_item_quantity}    Get Web Value    xpath=(//input[contains(@id, "txt-AddToCartQty")])[${i}]
    \    ${actual_cart_quantity}    Evaluate    ${actual_cart_quantity} + ${get_item_quantity}
    Capture Page Screenshot
    Should Be Equal As Integers    ${expect_cart_quantity}    ${actual_cart_quantity}
