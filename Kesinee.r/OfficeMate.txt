*** Settings ***
Resource         Resource/PageKeyword.txt

*** Test Cases ***
TH_CanonCalculator_TheLowestPrice
   #Set Data
   ${Language}   BuiltIn.Set Variable   ${Language_TH}
   #Go To Office Mate
   Open Browser   ${URL_OfficeMate}   ff
   ClosePopUpGetCode
   SelectLanguage   ${Language}
   SearchProduct   canon calculator
   FilterByBrand   ${Band_Canon}
   FilterByCategories   เครื่องคิดเลข   เครื่องคิดเลขตั้งโต๊ะ พกพา   เครื่องคิดเลขวิทยาศาสตร์
   SelectSortByLowToHigh
   ${Dict_OrderTheFirstProduct}   SelectTheFirstProductLowestPriceInCart   ${Qty_1}
   Go Back
   ${Dict_OrderTheSecondProduct}   SelectTheSecondProductLowestPriceInCart   ${Qty_1}
   ${List_Price}   CheckAllOrderInCart   ${Language}   ${Dict_OrderTheFirstProduct}   ${Dict_OrderTheSecondProduct}
   VerifySummaryAmount   ${List_Price}
   [Teardown]   Close All Browsers

EN_CanonCalculator_TheLowestPrice
   #Set Data
   ${Language}   BuiltIn.Set Variable   ${Language_EN}
   #Go To Office Mate
   Open Browser   ${URL_OfficeMate}   ff
   ClosePopUpGetCode
   SelectLanguage   ${Language}
   SearchProduct   canon calculator
   FilterByBrand   ${Band_Canon}
   FilterByCategories   Calculator & Tools   Portable Calculator
   SelectSortByLowToHigh
   ${Dict_OrderTheFirstProduct}   SelectTheFirstProductLowestPriceInCart   ${Qty_1}
   Go Back
   ${Dict_OrderTheSecondProduct}   SelectTheSecondProductLowestPriceInCart   ${Qty_1}
   ${List_Price}   CheckAllOrderInCart   ${Language}   ${Dict_OrderTheFirstProduct}   ${Dict_OrderTheSecondProduct}
   VerifySummaryAmount   ${List_Price}
   [Teardown]   Close All Browsers
