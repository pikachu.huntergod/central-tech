*** Settings ***
Library           Selenium2Library
Library           String
Library           OperatingSystem
Resource          Redefine_Keywords.txt

*** Keywords ***
wOpenBrowserOfficemate
    Selenium2Library.Open Browser    https://www.officemate.co.th    ff
    Selenium2Library.Maximize Browser Window
    ${StatusPopup}    Run Keyword And Return Status    Selenium2Library.Wait Until Element Is Visible    //span[@class='_2p_6G']    2s
    Run Keyword If    '${StatusPopup}' == 'True'    Click Web Element    //span[@class='_2p_6G']    2s
    Selenium2Library.Wait Until Element Is Visible    //a[@class='_33BvM _3PR4h']    2s

wSearchItem
    [Arguments]    ${Item}
    Click Web Element    //input[@class='_5vxBY _32CNv _2MLml']    2s
    Input Text    //input[@class='_5vxBY _32CNv _2MLml']    ${Item}
    Click Web Element    //div[@id='btn-searchResultPage']    2s

wSelectLanguate
    [Arguments]    ${Language}
    ${Language}    Convert To Lowercase    ${Language}
    Click Web Element    //div[@class='_2fJQI'][text()='${Language}']    2s
    Comment    Assign Id To Element    //body/div[@id='app']//div[@class='_14IZ-']//div[@class='sFwnt'][2]/div[@class='_5cAvb']/div[@class='_2fJQI _2C1o9'][text()='${Language}']    idLanguage
    Comment    Execute Javascript    document.querySelector("idLanguage").click()

wSelectBand
    [Arguments]    ${Band}
    Click Web Element    //div[@id='btn-collapseClose-Brand']/..//div[@class='_37hmz _1W4v4']//div[text()='${Band}']    2s

wAddItemToCart
    [Arguments]    ${ItemAmount}
    Selenium2Library.Wait Until Page Contains Element    //div[@data-testid='pnl-productGrid']//div[@class='yrjND'][1]//div[@class='xmOSN _34nP2']//div[@class='_2KWoO']    2s
    Mouse Over    //div[@data-testid='pnl-productGrid']//div[@class='yrjND'][1]//div[@class='xmOSN _34nP2']//div[@class='_2KWoO']
    : FOR    ${i}    IN RANGE    0    ${ItemAmount}
    \    Click Web Element    //div[@data-testid='pnl-productGrid']//div[@class='yrjND'][1]//div[@class='xmOSN _34nP2']//div[@class='_2KWoO']//span[@class='AddToCartButton__AddCartTxt-sc-1wy62u8-0 kwYWfS']    2s
    [Return]    ${ItemAmount}

wSelectSortBy
    [Arguments]    ${SortBy}
    Click Web Element    //div[@class='_3pSVv _19-Sz F0sHG _1eAL0']//div[@class='_13F3t _2cV8K x1C8Q MGtP6']//div[text()='Sort by']    2s
    Click Web Element    //div[@class='_1Qw5s Uh6-P']//div[@class='_2CEQo'][text()='${SortBy}']    2s

wVerifyItemAmountCart
    [Arguments]    ${ItemAmount}
    sleep    2s
    ${GetAmount}    Get Web Value    //div[@class='_33WrF _2Gu9M']//input[@id='txt-AddToCartQty-OFM8015945']    5s
    Should Be Equal    ${ItemAmount}    ${GetAmount}

wClickViewCart
    Click Web Element    //div[@data-testid='btn-MiniCart']//span[@id='lbl-minicartQty']    2s
    Click Web Element    //a[@id='lnk-minicartToCartPage']//div[text()='View Cart']    2s
