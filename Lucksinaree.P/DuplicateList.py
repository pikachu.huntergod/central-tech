import random

# Generate random list range 1000 
randomList = random.sample(range(1, 1001), 1000)

# Add 2 Duplicate Data
for index in range (0,2):
    randomIndex = random.randint(1, 1000)
    randomList.append(randomList[randomIndex])

lenRandomList = len(randomList)

print ('====== randomList Data ======')
print ('lenght :' ,lenRandomList)
print ('List :' ,randomList)

# Check Duplicate Data
listUnseen = []
listSeen = []

for item in randomList:
    if item not in listUnseen:
        listUnseen.append(item)
    else:
        listSeen.append(item)

lenListSeen= len(listSeen)

print ('===== duplicateList Data ======')
print ('lenght :' ,listSeen)
print ('List :' ,lenListSeen)
