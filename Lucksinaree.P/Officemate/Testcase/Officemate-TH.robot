*** Settings ***
Resource          ../Resources/Keywords.resource
Variables         CalulatorTestVar.py    ${LANG}    # th_TH | en_US

*** Variables ***
${LANG}           th_TH
${maxTIME}        10s
${minTIME}        5s

*** Test Cases ***
Calculator-LowerestPrice
    Open Browser Office Mate
    Handle Popup
    Select Language    ${LANG}
    # Product Calculator
    Go To Category Screen    ${mainCategory}    ${subCategory}
    Product Filter    ${dictProduct}
    Capture Result    jquery:[data-testid="pnl-productGrid"]    product-result.png
    # Verify Grid Product
    Verify Grid Product    1    ${expectedProduct}[1][name]    ${expectedProduct}[1][price]
    Verify Grid Product    2    ${expectedProduct}[2][name]    ${expectedProduct}[2][price]
    # Add To Cart
    # Row 1 = Estimate Delivery
    Add Product Grid to Cart By Position    1
    Add Product Grid to Cart By Position    2
    # View Cart
    Go To View Cart Screen
    Verify Grid Cart    2    ${expectedProduct}[1][name]    ${expectedProduct}[1][price]
    Verify Grid Cart    3    ${expectedProduct}[2][name]    ${expectedProduct}[2][price]
    Capture Result    jquery:._1bjSn>div:nth-child(1)    cart-result.png
    Close Browser
