def get_variables(lang = 'en_US'):

    if lang == 'th_TH':
        data = {'mainCategory': 'อุปกรณ์สำนักงาน',
               'subCategory': 'เครื่องคิดเลข',
               'dictProduct':{
                   'filter':{'Brand':'Canon'},
                   'sortBy':'ราคา ต่ำ > สูง'
                    },
                'expectedProduct':{
                    '1':{'name':'เครื่องคิดเลข สีชมพู Canon LS-88Hi III','price':'฿ 198.00'},
                    '2':{'name':'เครื่องคิดเลข สีม่วง Canon LS-88Hi III','price':'฿ 198.00'}
                    }
               }
    else:
        data = {'mainCategory': 'Office Supplies',
               'subCategory': 'Calculator & Tools',
               'dictProduct':{
                   'filter':{'Brand':'Canon'},
                   'sortBy':'price low > high'
                   },
                    
                'expectedProduct':{
                    '1':{'name':'Canon LS-88Hi III Calculator Pink','price':'฿ 198.00'},
                    '2':{'name':'Calculator Purple Canon LS-88Hi III','price':'฿ 198.00'}
                   }
                }
    return data