*** Settings ***
Library           Selenium2Library
Library           Collections

*** Keywords ***
Search
    [Arguments]    ${item}
    Wait Until Page Contains Element    //input[@data-testid="txt-SearchBar"]
    Input Text    //input[@data-testid="txt-SearchBar"]    ${item}
    wClickElement    //div[@id="btn-searchResultPage"]

Open Website
    [Arguments]    ${url}
    Open Browser    ${url}    gc
    Maximize Browser Window
    ${result}    Run Keyword And Return Status    Wait Until Page Contains Element    //span[text()='X'][not(contains(@class,'MD'))]
    Run Keyword If    ${result}==${True}    wClickElement    //span[text()='X'][not(contains(@class,'MD'))]

wClickElement
    [Arguments]    ${locator}
    Wait Until Page Contains Element    ${locator}    10s
    Click Element    ${locator}

Sort By Low To High Price
    wClickElement    //div[@id="btn-goBackProductCompare"]/following-sibling::div/.//div[text()='จัดเรียงตาม']
    wClickElement    //div[@class='_1V_Pj']//div[@id='price_asc']

Add To Cart And Verify
    [Arguments]    ${numberOfProduct}
    Wait Until Page Contains Element    (//a[contains(@id,'lbl-ProductPreview-Name')])[1]    10s
    # add to cart
    ${expectedProduct}    Create List
    : FOR    ${i}    IN RANGE    1    ${numberOfProduct}+1
    \    ${p}    Get Element Attribute    (//a[contains(@id,'lbl-ProductPreview-Name')])[${i}]    data-pid
    \    Append To List    ${expectedProduct}    ${p}
    \    Mouse Over    (//a[contains(@id,'lbl-ProductPreview-Name')])[${i}]
    \    wClickElement    (//div[contains(@id,"btn-addCart")])[${i}]/span[text()='ใส่ในตะกร้า']
    wClickElement    //div[@data-testid="btn-MiniCart"]
    wClickElement    //a[@id='lnk-minicartToCartPage']/div
    #in cart
    Wait Until Page Contains Element    //div[contains(@id,"btn-addToCartDecrementQty")]
    ${actualProduct}    Create List
    : FOR    ${i}    IN RANGE    1    ${numberOfProduct}+1
    \    Wait Until Page Contains Element    (//div[contains(@id,"btn-addToCartDecrementQty")])[${i}]
    \    ${p}    Get Element Attribute    (//div[contains(@id,"btn-addToCartDecrementQty")])[${i}]    data-pid
    \    Append To List    ${actualProduct}    ${p}
    #verify
    Lists Should Be Equal    ${expectedProduct}    ${actualProduct}

Check Band And Category
    [Arguments]    ${brand}    ${item}
    wClickElement    //div[text()='${item}'][1]
    wClickElement    //div[text()='${brand}']
