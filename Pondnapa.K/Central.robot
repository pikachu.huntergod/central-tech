*** Settings ***
Resource          Keywords.robot

*** Test Cases ***
01_GetDuplicateNumber
    #mock array list 5+2 index
    ${arrayList}    Create List    1    4    3    2    5
    Append To List    ${arrayList}    2    3
    #order list
    Sort List    ${arrayList}
    log    ${arrayList}
    # find duplicate
    ${count}    Get Length    ${arrayList}
    ${duplicate}    Create List
    : FOR    ${i}    IN RANGE    0    ${count}
    \    Continue For Loop If    ${i}==0
    \    ${cur}    Set Variable    ${arrayList[${i}]}
    \    ${pre}    Set Variable    ${arrayList[${i-1}]}
    \    ${status}    Run Keyword And Return Status    Should Be Equal As Integers    ${cur}    ${pre}
    \    Run Keyword If    ${status}==${True}    Append To List    ${duplicate}    ${cur}
    log    ${duplicate}

02_VerifyAddToCart
    Open Website    https://www.officemate.co.th
    Search    canon calculator
    Sort By Low To High Price
    Check Band And Category    Canon    เครื่องคิดเลข
    Add To Cart And Verify    2
    Close Browser
