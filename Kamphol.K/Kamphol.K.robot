*** Settings ***
Library           Selenium2Library
Library           Collections
Resource          Locator.robot
Library           String

*** Variables ***

*** Test Cases ***
01_Find_Duplicate_Number
    ${Expect_Number}    GenerateNumber
    Log List    ${Expect_Number}
    ${TotalDuplicateNumber}    FindDuplicateNumber    ${Expect_Number}
    Log List    ${TotalDuplicateNumber}

02_Select_Canon_calculator_Lowest_Price
    Web_OpenOfficemateWebsite
    Web_ClosePopUp
    Web_SearchProduct    CANON
    Web_SelectCategoryCalculator
    Web_SortingByLowestPrice
    Web_SelectFirstItem
    Web_SelectSecondItem
    Web_VerifyItemInCartPage
    [Teardown]    Close All Browsers

*** Keywords ***
FindDuplicateNumber
    [Arguments]    ${ITEM}
    ${TotalNumber}    Get Length    ${ITEM}
    ${TotalDuplicateNumber}    Create List
    log    ${TotalNumber}
    : FOR    ${index}    IN RANGE    0    ${TotalNumber}
    \    ${DuplicateNumber}    CheckWithExpectNumber    ${TotalNumber}    ${index}    ${ITEM[${index}]}    ${ITEM}
    \    Run Keyword If    '${DuplicateNumber}'!='None'    Append To List    ${TotalDuplicateNumber}    ${DuplicateNumber}
    log    ${TotalDuplicateNumber}
    [Return]    ${TotalDuplicateNumber}

CheckWithExpectNumber
    [Arguments]    ${Total}    ${Postition}    ${Number}    ${ListOfNumber}
    ${DuplicateNumber}    Set Variable    ${None}
    : FOR    ${index}    IN RANGE    ${Postition}+1    ${Total}
    \    ${DuplicateNumber}    Run Keyword If    ${Number} ==${ListOfNumber[${index}]}    Set Variable    ${ListOfNumber[${index}]}
    \    Exit For Loop If    ${Number} ==${ListOfNumber[${index}]}
    [Return]    ${DuplicateNumber}

GenerateNumber
    ${DuplicateNumber}    Evaluate    random.sample(range(1, 1001), 2)    random
    ${ThousandNumber}    Evaluate    random.sample(range(1, 1001), 1000)    random
    ${Expect_Number}    Combine Lists    ${DuplicateNumber}    ${ThousandNumber}
    [Return]    ${Expect_Number}

Web_WaitAndClickElement
    [Arguments]    ${Locator}    ${TimeOut}=60
    Wait Until Element Is Visible    ${Locator}    ${TimeOut}
    Click Element    ${Locator}

Web_OpenOfficemateWebsite
    [Arguments]    ${Browser}=gc
    Open Browser    https://www.officemate.co.th/th    ${Browser}
    Selenium2Library.Maximize Browser Window
    sleep    10
    Comment    Wait Until Element Is Visible    xpath=//div[@id="pnl-SubScription-Popup"]
    Comment    Click Element    xpath=//div[@id="pnl-SubScription-Popup"]/div[@class="_1UAKn"][2]/span    #ปุ่มปิด pop up
    Comment    Wait Until Element Is Enabled    xpath=//*[@placeholder="ค้นหาสินค้าหรือรหัส"]    60
    Comment    Input Text    xpath=//*[@placeholder="ค้นหาสินค้าหรือรหัส"]    CANON
    Comment    Wait Until Element Is Enabled    xpath=//div[@id="btn-searchResultPage"]
    Comment    Click Element    xpath=//div[@id="btn-searchResultPage"]
    Comment    Wait Until Element Is Enabled    xpath=//*[@id="btn-collapseClose-Category"]/../div[2]//div[text()="เครื่องคิดเลข"]
    Comment    Click Element    xpath=//*[@id="btn-collapseClose-Category"]/../div[2]//div[text()="เครื่องคิดเลข"]
    Comment    Wait Until Element Is Enabled    xpath=//div[@id="btn-goBackProductCompare"]/../div[3]//div[text()="จัดเรียงตาม"]
    Comment    Click Element    xpath=//div[@id="btn-goBackProductCompare"]/../div[3]//div[text()="จัดเรียงตาม"]
    Comment    Wait Until Element Is Enabled    xpath=//div[@id="btn-goBackProductCompare"]/../div[3]//div[@id="price_asc"]
    Comment    Click Element    xpath=//div[@id="btn-goBackProductCompare"]/../div[3]//div[@id="price_asc"]
    Comment    Wait Until Element Is Enabled    xpath=//div[@data-testid="pnl-productGrid"]/div[1]//span[text()="ใส่ในตะกร้า"]    #Item จาก Position 1
    Comment    Click Element    xpath=//div[@data-testid="pnl-productGrid"]/div[1]//span[text()="ใส่ในตะกร้า"]    #Item จาก Position 1
    Comment    Wait Until Element Is Enabled    xpath=//div[@data-testid="pnl-productGrid"]/div[2]//span[text()="ใส่ในตะกร้า"]    #Item จาก Position 2
    Comment    Click Element    xpath=//div[@data-testid="pnl-productGrid"]/div[2]//span[text()="ใส่ในตะกร้า"]    #Item จาก Position 2
    Comment    ${TotleItem}    Get Text    xpath=//span[@id="lbl-minicartQty"]
    Comment    Should Be Equal    ${TotleItem}    2
    Comment    Wait Until Element Is Enabled    xpath=//div[@data-testid="btn-MiniCart"]
    Comment    Click Element    xpath=//div[@data-testid="btn-MiniCart"]
    Comment    Wait Until Element Is Enabled    xpath=//a[@id="lnk-minicartToCartPage"]
    Comment    Click Element    xpath=//a[@id="lnk-minicartToCartPage"]
    Comment    Wait Until Element Is Visible    xpath=//div[@id="cart-items"]//*[@class="_1bjSn"]/div
    Comment    sleep    5    #Workaround
    Comment    ${CountItem}    Get Element Count    xpath=//div[@id="cart-items"]//*[@class="_1bjSn"]/div    #ไม่น่าใช้ class แต่ไม่มีตัวเลือกที่ดีๆเลย
    Comment    log    ${CountItem-1}
    Comment    sleep    10

Web_ClosePopUp
    Web_WaitAndClickElement    ${Button_ClosePopUp}

Web_SearchProduct
    [Arguments]    ${Text}
    Wait Until Element Is Enabled    ${Textbox_SearchProduct}    60
    Input Text    ${Textbox_SearchProduct}    ${Text}
    Web_WaitAndClickElement    ${Button_SearchProduct}

Web_SelectCategoryCalculator
    Web_WaitAndClickElement    ${Button_CalculatorCetagory}

Web_SortingByLowestPrice
    Web_WaitAndClickElement    ${Button_Sorting}
    Web_WaitAndClickElement    ${Button_LowestPrice}

Web_SelectFirstItem
    Sleep    5    #workaround wait foe web re-calculate item
    Wait Until Element Is Enabled    ${Button_AddToCartFirstPosition}
    Mouse Over    ${Button_AddToCartFirstPosition}
    Web_WaitAndClickElement    ${Button_AddToCartFirstPosition}

Web_SelectSecondItem
    Sleep    5    #workaround wait foe web re-calculate item
    Wait Until Element Is Enabled    ${Button_AddToCartSecondPosition}
    Mouse Over    ${Button_AddToCartSecondPosition}
    Web_WaitAndClickElement    ${Button_AddToCartSecondPosition}

Web_VerifyItemInCartPage
    ${TotleItem}    Get Text    ${Icon_ItemInCart}
    Should Be Equal    ${TotleItem}    2
    Web_WaitAndClickElement    ${Button_Cart}
    Web_WaitAndClickElement    ${Button_ViewCartPage}
    Wait Until Element Is Visible    ${ItemInCart}
    sleep    5    #Workaround
    ${CountItem}    Get Element Count    ${ItemInCart}
    log    ${CountItem-1}    #Workaround เนื่องจากItem ที่ Get ได้จาก Xpath อ้่งจาก Class ทำให้ได้ส่วน HEader มาด้วย
    Capture Page Screenshot
