*** Variables ***
${PopUp}          xpath=//div[@id="pnl-SubScription-Popup"]
${Button_ClosePopUp}    xpath=//div[@id="pnl-SubScription-Popup"]/div[@class="_1UAKn"][2]/span
${Textbox_SearchProduct}    xpath=//*[@placeholder="ค้นหาสินค้าหรือรหัส"]
${Button_SearchProduct}    xpath=//div[@id="btn-searchResultPage"]
${Button_CalculatorCetagory}    xpath=//*[@id="btn-collapseClose-Category"]/../div[2]//div[text()="เครื่องคิดเลข"]
${Button_Sorting}    xpath=//div[@id="btn-goBackProductCompare"]/../div[3]//div[text()="จัดเรียงตาม"]
${Button_LowestPrice}    xpath=//div[@id="btn-goBackProductCompare"]/../div[3]//div[@id="price_asc"]
${Button_AddToCartFirstPosition}    xpath=//div[@data-testid="pnl-productGrid"]/div[1]//span[text()="ใส่ในตะกร้า"]
${Button_AddToCartSecondPosition}    xpath=//div[@data-testid="pnl-productGrid"]/div[2]//span[text()="ใส่ในตะกร้า"]
${Icon_ItemInCart}    xpath=//span[@id="lbl-minicartQty"]
${Button_Cart}    xpath=//div[@data-testid="btn-MiniCart"]
${Button_ViewCartPage}    xpath=//a[@id="lnk-minicartToCartPage"]
${ItemInCart}     xpath=//div[@id="cart-items"]//*[@class="_1bjSn"]/div    # ไม่น่าใช้ class แบบนี้แต่ไม่มีตัวเลือกอื่นที่ unique พอ
