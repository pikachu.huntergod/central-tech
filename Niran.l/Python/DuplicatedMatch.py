from random import randint
import collections

def mylist():
    list = []
    for _ in range(100):
        value = randint(1, 1000)
        list.append(value)
    return list

newList = mylist()

print(newList)
print([item for item, count in collections.Counter(newList).items() if count > 1])