*** Settings ***
Resource          Resoure/PageKeywords.txt
Resource          Resoure/PageVariables.txt

*** Variables ***

*** Test Cases ***
ArrayNumberFix
    ${ListNumber}    GenerateNumberFix
    log    ${ListNumber}
    FindNumberDuplicateFix    ${ListNumber}

OfficeMate
    OpenBrowserOfficeMateWebsite    ${OfficemateURL}
    VerifyPopUpAdvertisement
    #Find Canon Calculator And Lowest price
    SelectLanguage
    SearchCanonCalculator    ${CanonCalculator}
    #Select Canon LS-88Hi III Calculator Pink
    TheFirstSelectCanonCalculator
    #Select Calculator Purple Canon LS-88Hi III
    TheSecondsSelectCanonCalculator
    #Verify \ there are 2 items in cart
    VerifyCanonCalculator2ItemsInCart
    [Teardown]    SeleniumLibrary.Close Browser

ArrayNumberRandom
    #RandomNumber
    ${ListNumber}    GenerateNumberRanDom
    log    ${ListNumber}
    FindNumberDuplicateRandom    ${ListNumber}

*** Keywords ***
