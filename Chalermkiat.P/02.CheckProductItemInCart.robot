*** Settings ***
Library           Selenium2Library
Library           Collections

*** Variables ***
${SeleniumTimeOut}    10s
@{ListOfProductId}

*** Test Cases ***
01
    Open Browser Officemate
    Input Calculator
    Choose Categories Calculator
    Check Band Canon
    Sort By Price Low > High
    Add 2 Lowest Price Into Cart
    Open Cart
    Verify 2 Product Item
    Close Browser Officemate

*** Keywords ***
Open Browser Officemate
    Open Browser    https://www.officemate.co.th    GC    alias=officemate
    Wait Until Element Is Visible And Click Element    (//div[@id='pnl-SubScription-Popup']//span[text()='X'])[2]
    Maximize Browser Window
    #Choose Eng
    Wait Until Element Is Visible And Click Element    //div[text()='en']

Wait Until Element Is Visible And Click Element
    [Arguments]    ${locator}
    Wait Until Element Is Visible    ${locator}    ${SeleniumTimeOut}
    Click Element    ${locator}

Wait Until Element Is Visible And Input Text
    [Arguments]    ${locator}    ${text}
    Wait Until Element Is Visible    ${locator}    ${SeleniumTimeOut}
    Input Text    ${locator}    ${text}

Wait Until Element Is Visible And Mouse Over
    [Arguments]    ${locator}
    Wait Until Element Is Visible    ${locator}    ${SeleniumTimeOut}
    Mouse Over    ${locator}

Add 2 Lowest Price Into Cart
    ${listOfProductId}    Create List
    : FOR    ${index}    IN RANGE    1    3
    \    ${tempProductId}    Wait Until Keyword Succeeds    3x    1s    Add Product To Cart    ${index}
    \    Append To List    ${listOfProductId}    ${tempProductId}
    Set Test Variable    ${ListOfProductId}    ${listOfProductId}

Input Calculator
    Wait Until Element Is Visible And Input Text    //input[@data-testid='txt-SearchBar']    calcuator

Choose Categories Calculator
    Wait Until Element Is Visible And Click Element    //div[normalize-space(text())='in Portable Calculator']

Check Band Canon
    Wait Until Element Is Visible And Click Element    //div[text()='Canon']

Sort By Price Low > High
    Wait Until Element Is Visible And Click Element    //div[@id='category-page-content']//div[text()='Sort by']
    Wait Until Element Is Visible And Click Element    //div[@id='category-page-content']//div[text()='price low > high']

Add Product To Cart
    [Arguments]    ${index}
    Wait Until Element Is Visible And Mouse Over    (//a[@data-product-brand='Canon'][starts-with(@id,'lbl-ProductPreview-Name')])[${index}]
    ${tempId}    Get Element Attribute    (//a[@data-product-brand='Canon'][starts-with(@id,'lbl-ProductPreview-Name')])[${index}]    data-product-id
    Wait Until Element Is Visible And Click Element    (//a[@data-product-brand='Canon'][starts-with(@id,'lbl-ProductPreview-Name')])[${index}]/../../..//span[text()='Add to cart']
    Return From Keyword    ${tempId}

Verify 2 Product Item
    : FOR    ${productId}    IN    @{ListOfProductId}
    \    Wait Until Element Is Visible    //div[@id='cart-items']//span[not(@class)][normalize-space(.)='Product Code: ${productId}']
    END

Open Cart
    Wait Until Element Is Visible And Click Element    //span[@id='lbl-minicartQty']
    Wait Until Element Is Visible And Click Element    //div[text()='View Cart']

Close Browser Officemate
    Switch Browser    officemate
    Close Browser
