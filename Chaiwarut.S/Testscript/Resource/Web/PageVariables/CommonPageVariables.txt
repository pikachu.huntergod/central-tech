*** Settings ***
Resource          CommonPageVariables.txt

*** Variables ***
${General Timeout}    10s
## URL
${Url_CPWM}       https://staging-cpwm.intra.ais/
## UserName
${Username_Pochaman}    pochaman
${OriginalPassword_Pochaman}    Poc#1048
${PINEmpID_Pochaman}    00001048
## Path File
${FirefoxProfilePath}    ${CURDIR}\\..\\..\\..\\..\\FirefoxProfile
${Repository_Path}    ${CURDIR}\\..\\..\\..\\..\\
${ExportFile_Folder}    \\ExportFile
${RepositoryFileExcel_Path}    ${CURDIR}\\..\\..\\
${Document_Folder}    Document
${GeneratePasswordAndPasscode_ExcelFile}    \\Generate Password AND Passcode
${Url_BackOfficeCPWM}    https://staging-cpwm.intra.ais:8443
${ResetPasscode}    ResetPasscode
${RequestTemporaryPassword}    RequestTemporaryPassword
${ChangePassword}    ChangePassword
${Status_False}    False
${Status_True}    True
## OfficeMate
${NameAtoZ}       name_asc
${NameZtoA}       name_desc
${PriceLowest}    price_asc
${PriceHighest}    price_dasc
${BrandNameAtoZ}    brand_name_asc
${BrandNameZtoA}    brand_name_asc
${Url_OfficeMate}    https://www.officemate.co.th
