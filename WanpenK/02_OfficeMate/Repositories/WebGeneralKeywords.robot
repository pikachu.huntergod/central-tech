*** Settings ***
Library           Selenium2Library
Library           String
Library           DateTime
Resource          WebGeneralVariable.robot
Resource          WebGeneralRepository.robot
Library           Collections

*** Keywords ***
Web Verify Access Successfully
    [Documentation]    _Created by : WanpenK_
    ...    _Create date : 05-Jan-18_
    Wait Until Page Contains Element    Xpath=//*[@id="app"]/div[2]/div[2]/div[2]/div[1]/div/a    ${Timeout}
    Web Check Access Popup
    Click Element    xpath=//*[@id="app"]/div[2]/div[2]/div[1]/div/div[2]/div[3]/div[3]
    Click Element    xpath=//*[@href="/th/office-supplies"]

Web Access
    [Arguments]    ${Access_URL}
    ${options}=    Evaluate    sys.modules['selenium.webdriver.chrome.options'].Options()    sys
    Call Method    ${options}    add_argument    --disable-notifications
    ${driver}=    Create Webdriver    Chrome    options=${options}
    sleep    2
    go to    ${Access_URL}
    maximize browser window

Web Check Access Popup
    ${popup}=    Run Keyword And Return Status    Wait Until Page Contains Element    xpath=//*[@id="pnl-SubScription-Popup"]/descendant::div[1]/img    ${Timeout}
    Run Keyword If    '${popup}'=='True'    Click Element    xpath=//*[@id="pnl-SubScription-Popup"]/div[2]/span

Web Check Promotion Popup
    ${popup}=    Run Keyword And Return Status    Wait Until Page Contains Element    xpath=//*[@id="img-image-1582195469045"]    ${Timeout}
    Run Keyword If    '${popup}'=='True'    Click Element    xpath=//*[@id="icon-close-button-1454703945249"]

Web Find Goods
    [Arguments]    ${goods}
    Click Element    xpath=//*[@id="app"]/div/div[2]/div[1]/div/div[2]/div[3]/div[3][contains(.,"th")]
    FOR    ${index}    IN RANGE    1    3
        ${check_goods}=    Run Keyword And Return Status    Element Should Contain    xpath=//*[@id="category-page"]/div[1]/div/div/div[1]    ${goods}
        Run Keyword If    '${check_goods}'=='False'    Click Element    xpath=//*[@id="category-page"]/div[1]/div/div/div[1]/div[3]/div[contains(.,"เพิ่มเติม")]
        ...    ELSE    Click Element    xpath=//*[@id="category-page"]/div[1]/div/div/div[1]/div[2]/*[contains(.,"${goods}")]
    END
    sleep    3

Web Spec Goods
    [Arguments]    ${group}    ${brand}
    Capture Page Screenshot
    Wait Until Page Contains Element    xpath=//*[@id="category-page-content"]/div[2]/div[1]    ${Timeout}    #หมวดหมู่สินค้า
    Click Element    xpath=//*[@id="app"]/div/div[2]/div[1]/div/div[2]/div[3]/div[3][contains(.,"th")]
    sleep    3
    Scroll Element Into View    xpath=//*[@id="btn-collapseClose-จำนวนหลัก"]
    sleep    3
    ${total_amounts}=    Get text    xpath= //*[@id="category-page-content"]/div[2]/div[1]/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div[1][contains(.,"${group}")]/parent::*/div[2]
    Click Element    xpath=//*[@id="category-page-content"]/div[2]/div[1]/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div[1][contains(.,"${group}")]/parent::*/parent::*/parent::*/child::div[1]
    Wait Until Page Contains Element    xpath=//*[@id="category-page-content"]/div[2]/div[1]/div/div[1]/div/div[2]/div/div[1]/div/div[1]/img[@src="/icons/checked.svg"]    ${Timeout}
    sleep    3
    Scroll Element Into View    xpath=//*[@id="btn-collapseClose-จำนวนหลัก"]
    sleep    3
    Scroll Element Into View    xpath=//*[@id="btn-collapseClose-Brand"]
    ${brand_amounts}=    Get text    xpath=//*[@id="category-page-content"]/div[2]/div[1]/div/div[2]/div/div[2]/div/div[1]/div/div[2]/div/div[1][contains(.,"${brand}")]/parent::*/child::div[2]
    Click Element    xpath=//*[@id="category-page-content"]/div[2]/div[1]/div/div[2]/div/div[2]/div/div[1]/div/div[2]/div/div[1][contains(.,"${Brand}")]/parent::*/parent::*/parent::*/child::div[1]
    sleep    3
    Scroll Element Into View    xpath=//*[@id="btn-collapseClose-จำนวนหลัก"]
    Wait Until Page Contains Element    xpath= //*[@id="category-page-content"]/div[2]/div[1]/div/div[3]/div/div[2]/div/div[1]/div/div[1]/img[@src="/icons/checked.svg"]    ${Timeout}
    Capture Page Screenshot
    [Return]    ${brand_amounts}    # ${total_amounts} |

Web Scroll to Element
    [Arguments]    ${locator}
    [Documentation]    - Created by : Wanpen.K
    ...    - Create date : 3-Jun-19
    Wait Until Page Contains Element    ${locator}    20s
    ${target}    Get Vertical Position    ${locator}
    ${width}    ${height}    Selenium2Library.Get Element Size    ${locator}
    ${element}    Evaluate    ${target}-150
    Run Keyword If    ${element}<0    Execute JavaScript    window.scrollTo(0, 0)
    ...    ELSE    Execute JavaScript    window.scrollTo(0, ${element})

Web Find Lowest Price
    ${getprice_loop}=    Get Element Count    xpath=//*[@data-testid="pnl-productGrid"]/child::div[@class="yrjND"]
    ${getprice_loop_max}=    Evaluate    ${getprice_loop}+1
    ${dict1}=    Create Dictionary
    ${dict2}=    Create Dictionary
    FOR    ${index}    IN RANGE    1    ${getprice_loop_max}
        sleep    3
        Scroll Element Into View    xpath=//*[@data-testid="pnl-productGrid"]/child::div[@class="yrjND"][${index}]/descendant::div[contains(@id, 'lbl-ProductList-Price')]
        ${check_outofstock}=    Run Keyword And Return Status    Page Should Not Contain Element    xpath=//*[@data-testid="pnl-productGrid"]/child::div[@class="yrjND"][${index}]/descendant::div[@data-testid="pnl-Badge"]
        Run Keyword If    '${check_outofstock}'=='True'    Web Find Lowest Price-Detail    ${index}    ${dict1}    ${dict2}
        log    ${dict1}
        log    ${dict2}
    END
    log    ${dict1}
    log    ${dict2}
    [Return]    ${dict1}    ${dict2}

Web Find Lowest Price-Detail
    [Arguments]    ${index}    ${dict1}    ${dict2}
    ${model}=    Get text    xpath=//*[@data-testid="pnl-productGrid"]/child::div[@class="yrjND"][${index}]/descendant::h2/child::a
    ${code}=    Get text    xpath=//*[@data-testid="pnl-productGrid"]/child::div[@class="yrjND"][${index}]/descendant::div[@class="ERPfP"]/child::div[2]
    ${txtprice}=    Get text    xpath=//*[@data-testid="pnl-productGrid"]/child::div[@class="yrjND"][${index}]/descendant::div[contains(@id, 'lbl-ProductList-Price')]
    ${price}=    Get Substring    ${txtprice}    2    20
    Run Keyword If    '${index}'=='1'    Set To Dictionary    ${dict1}    dict1_model    ${model}    dict1_code    ${code}    dict1_price    ${price}    dict1_index    ${index}
    Run Keyword If    '${index}'=='1'    Set To Dictionary    ${dict2}    dict2_model    ${model}    dict2_code    ${code}    dict2_price    ${price}    dict2_index    ${index}
    ${dict1_price}=    Get From Dictionary    ${dict1}    dict1_price
    ${dict2_price}=    Get From Dictionary    ${dict2}    dict2_price
    ${dict1_index}=    Get From Dictionary    ${dict1}    dict1_index
    ${result1}=    Run Keyword And Return Status    Should Be True    '${price}' < '${dict1_price}'
    Run Keyword If    '${result1}' == 'True'    Set To Dictionary    ${dict1}    dict1_model    ${model}    dict1_code    ${code}    dict1_price    ${price}    dict1_index    ${index}
    ${result2}=    Run Keyword And Return Status    Should Be True    '${price}' == '${dict1_price}' and '${index}' != '${dict1_index}'
    Run Keyword If    '${result2}' == 'True'    Set To Dictionary    ${dict2}    dict2_model    ${model}    dict2_code    ${code}    dict2_price    ${price}    dict2_index    ${index}
    ${result3}=    Run Keyword And Return Status    Should Be True    '${price}' < '${dict2_price}' and '${index}' != '${dict1_index}'
    Run Keyword If    '${result3}' == 'True'    Set To Dictionary    ${dict2}    dict2_model    ${model}    dict2_code    ${code}    dict2_price    ${price}    dict2_index    ${index}

Web Select Lowest Price
    [Arguments]    ${dict1}    ${dict2}
    Scroll Element Into View    xpath=//*[@id="btn-collapseClose-เงื่อนไขการแสดง"]
    ${dict1_index}    Get From Dictionary    ${dict1}    dict1_index
    ${dict2_index}    Get From Dictionary    ${dict2}    dict2_index
    ${lowestprice_list}=    Create list    ${dict1_index}    ${dict2_index}
    FOR    ${index}    IN RANGE    0    2
        sleep    3
        Web Scroll to Element    xpath=//*[@data-testid="pnl-productGrid"]/child::div[@class="yrjND"][${lowestprice_list[${index}]}]/descendant::span[contains(@class,"AddToCartButton")]
        sleep    3
        Click Element    xpath=//*[@data-testid="pnl-productGrid"]/child::div[@class="yrjND"][${lowestprice_list[${index}]}]/descendant::span[contains(@class,"AddToCartButton")]
        sleep    3
    END
    Capture Page Screenshot

Web Verify Order
    [Arguments]    ${dict1}    ${dict2}
    Click Element    xpath=//*[@id="lbl-minicartQty"]
    Wait Until Page Contains Element    xpath=//*[@id="lnk-minicartToCartPage"]    ${Timeout}
    Click Element    xpath=//*[@id="lnk-minicartToCartPage"]
    Wait Until Page Contains Element    xpath=//*[@id="lnk-breadcrumbs"]/descendant::*[@href="/th/cart"][contains(.,"ตะกร้าสินค้า")]    ${Timeout}
    sleep    3
    ${count_order}=    Get Element Count    xpath=//*[@id="add-by-sku"]/parent::*/following-sibling::div/child::div/child::div/child::div/child::div[1]/following-sibling::div
    Should Be Equal As Integers    ${count_order}    2
    ${max_loop}=    Evaluate    ${count_order}+1
    FOR    ${index}    IN RANGE    1    ${max_loop}
        ${round}=    Evaluate    ${index}+1
        ${name}=    Get Element Attribute    xpath=//*[@id="add-by-sku"]/parent::*/following-sibling::div/child::div/child::div/child::div/child::div[${round}]/child::div[1]/child::div/child::div/child::div[2]/child::div[1]/child::a    title
        ${code_txt}=    Get text    xpath=//*[@id="add-by-sku"]/parent::*/following-sibling::div/child::div/child::div/child::div/child::div[${round}]/child::div[1]/child::div/child::div/child::div[2]/child::div[2]/descendant::span/child::span
        ${code}=    Get Substring    ${code_txt}    12    30
        ${price_txt}=    Get text    xpath=//*[@id="add-by-sku"]/parent::*/following-sibling::div/child::div/child::div/child::div/child::div[${round}]/child::div[1]/child::div/child::div/child::div[5]/child::div
        ${price}=    Get Substring    ${price_txt}    2    20
        ${dict_name}=    Get From Dictionary    ${dict${index}}    dict${index}_model
        ${dict_code_txt}=    Get From Dictionary    ${dict${index}}    dict${index}_code
        ${dict_code}=    Get Substring    ${dict_code_txt}    6    30
        ${dict_price}=    Get From Dictionary    ${dict${index}}    dict${index}_price
        Should Be Equal    ${name}    ${dict_name}
        Should Be Equal    ${code}    ${dict_code}
        Should Be Equal    ${price}    ${dict_price}
    END
    Capture Page Screenshot
