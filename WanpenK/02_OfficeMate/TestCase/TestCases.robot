*** Settings ***
Resource          ../Repositories/WebGeneralKeywords.robot

*** Test Cases ***
02_OfficeMate_Find_lowestprice_calculator
    [Documentation]    2. Create an automated test script with the following steps. Any opensource tools and any programming language are ok. e.g. selenium, RBFW, cypress and so on. Once done, please upload the code to any public repository as you like and provide us the way to access the public repository.
    ...
    ...    Open https://www.officemate.co.th website
    ...    Find the canon calculator with the lowest price available on the officemate website
    ...    Add 2 lowest price calculators into cart
    ...    Open cart page and verify whether there are 2 items in cart
    ...
    ...
    ...    - ========================
    ...    - Created test script by : Wanpen.K
    ...    - Create date : May 18,2020
    ...    - ========================
    Web Access    ${URL}
    Web Verify Access Successfully
    Web Check Promotion Popup
    Web Find Goods    เครื่องคิดเลข
    ${brand_amounts}    Web Spec Goods    เครื่องคิดเลข    Canon
    ${dict1}    ${dict2}    Web Find Lowest Price
    Web Select Lowest Price    ${dict1}    ${dict2}
    Web Verify Order    ${dict1}    ${dict2}
    [Teardown]    close browser
